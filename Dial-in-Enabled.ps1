﻿$usernames = Get-ADUser -Filter * | select -ExpandProperty SamAccountName

foreach ($username in $usernames) {

    $dialin = Get-ADUser $username -Properties * | select -ExpandProperty msNPAllowDialin

    if ($dialin -eq "True") {
        Write-Output $username
    }
}