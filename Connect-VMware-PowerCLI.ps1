﻿#Import the VMware Module

Import-Module VMware.VimAutomation.Core

#Allow some time for the Module to load

Start-Sleep -Seconds 5

## Set Variables ##

#vCenter Target

[string]$vc = Read-Host -Prompt "Please enter the vCenter URL that you wish to connect to".Trim()

#Username 

$user = Get-Credential

##Connect to vCenter##

Connect-VIServer $vc -Credential $user