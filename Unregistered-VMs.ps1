﻿$credential = Get-Credential -Credential citrix\administrator
$cxsession = New-PSSession -ComputerName ase-sy-xdcont05.citrix.aseit.net -Credential $credential -ErrorAction SilentlyContinue
Import-PSSession $cxsession -ErrorAction SilentlyContinue
Enter-PSSession $cxsession -ErrorAction SilentlyContinue

Add-PSSnapin Citrix* -ErrorAction SilentlyContinue
$vms = Get-BrokerMachine | select MachineName, RegistrationState
$vmscount = $vms.Count
$unregvm = Get-BrokerMachine | where RegistrationState -eq "Unregistered" | Select HostedMachineName, AssociatedUserNames
$unregvmstring = $unregvm | Out-String
$unregvmcount = $unregvm.Count
if ($unregvmcount -ge 1)
{
exit
## Send PowerShell outputs to Telegram. NOTE: This requires PowerShell version 6.1 Preview.
#Import the PoshGram Module for communicating with the Telegram API
Import-Module "PoshGram"

#Setup Bot Token and Chat ID variables
$botToken = "607018310:AAHyKC65L3Kp225eqjdP0TXcQzOF7sbgrAA"
$chat = "-269979747"

#Send our Telegram alert.
Send-TelegramTextMessage -BotToken $botToken -ChatID $chat -Message "Good morning team. FYI, I've just finished polling Citrix, and out of $vmscount VMs, $unregvmcount are unregistered. Below is a list of the unregistered VMs..."
Send-TelegramTextMessage -BotToken $botToken -ChatID $chat -Message "$unregvmstring"

#{Send-MailMessage -From powershell@aseit.com.au -To mitchell.thomas@aseit.com.au -SmtpServer 10.1.1.80 -Subject "Citrix Alert: Unregistered VMs" -Body "Out of $vmscount VMs, $unregvmcount are unregistered. Below is a list of the unregistered VMs... $unregvmstring"
    }