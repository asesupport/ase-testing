﻿$cred = Get-Credential -Credential citrix\administrator
[string]$target = Read-Host -Prompt "Please enter an ASEC number"
$comp = "ase-sy-xdcont05.citrix.aseit.net"
Invoke-Command -ComputerName $comp -Credential $cred { Add-PSSnapin *citrix*;Get-BrokerMachine | ? MachineName -like "$target" | select MachineName,RegistrationState,AssociatedUserNames }